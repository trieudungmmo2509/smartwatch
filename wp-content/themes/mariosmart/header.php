    <!-- Mobile Sidenav -->
    <!DOCTYPE html>
    <html lang="en">

    <!-- Mirrored from deothemes.com/envato/neotech/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Apr 2021 15:53:32 GMT -->

    <head>
        <title>Neotech | Home</title>

        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="">

        <!-- Css -->
        <?php wp_head(); ?>
    </head>

    <body class=<?php echo is_home() ? "bg-darker" : ""; ?>>

        <!-- Preloader -->
        <div class="loader-mask">
            <div class="loader">
                <div></div>
            </div>
        </div>
        <!-- Bg Overlay -->
        <div class="content-overlay"></div>
        <style>
            #wpadminbar {
                display: none;
            }
        </style>
        <header class="sidenav" id="sidenav">
            <!-- Search -->
            <div class="sidenav__search-mobile">
                <form method="get" class="sidenav__search-mobile-form">
                    <input type="search" class="sidenav__search-mobile-input" placeholder="Search..." aria-label="Search input">
                    <button type="submit" class="sidenav__search-mobile-submit" aria-label="Submit search">
                        <i class="ui-search"></i>
                    </button>
                </form>
            </div>

            <nav>
                <ul class="sidenav__menu" role="menubar">
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'primary',
                            'container' => 'ul',
                            'menu_class' => 'nav__menu'
                        )
                    ); ?>
                </ul>
            </nav>

            <div class="socials sidenav__socials ">
                <a class="social-facebook" href="#" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                </a>
                <a class="social-twitter" href="#" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                </a>
                <a class="social-youtube" href="#" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                </a>
            </div>
        </header> <!-- end mobile sidenav -->

        <!-- Navigation -->
        <header class="nav">

            <div class="nav__holder nav--sticky">
                <div class="container relative">

                    <div class="flex-parent">

                        <!-- Mobile Menu Button -->
                        <button class="nav-icon-toggle" id="nav-icon-toggle" aria-label="Open mobile menu">
                            <span class="nav-icon-toggle__box">
                                <span class="nav-icon-toggle__inner"></span>
                            </span>
                        </button> <!-- end mobile menu button -->

                        <!-- Logo -->
                        <a href="index.html" class="logo">
                            <img class="logo__img" src="img/logo_light.png" srcset="img/logo_light.png 1x, img/logo_light@2x.png 2x" alt="logo">
                        </a>

                        <!-- Nav-wrap -->
                        <nav class="flex-child nav__wrap d-none d-lg-block">
                            <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'header-menu',
                                    'container' => 'ul',
                                    'menu_class' => 'nav__menu'
                                )
                            ); ?>
                        </nav> <!-- end nav-wrap -->



                        <!-- Nav Right -->
                        <div class="nav__right nav--align-right d-none d-lg-flex">
                            <!-- Search -->
                            <div class="nav__right-item nav__search">
                                <a href="#" class="nav__search-trigger" id="nav__search-trigger">
                                    <i class="ui-search nav__search-trigger-icon"></i>
                                </a>
                                <div class="nav__search-box" id="nav__search-box">
                                    <form class="nav__search-form">
                                        <input type="text" placeholder="Search an article" class="nav__search-input">
                                        <button type="submit" class="nav__search-button btn btn-md btn-color btn-button">
                                            <i class="ui-search nav__search-icon"></i>
                                        </button>
                                    </form>
                                </div>

                            </div>

                        </div> <!-- end nav right -->

                    </div> <!-- end flex-parent -->
                </div> <!-- end container -->

            </div>
        </header> <!-- end navigation -->