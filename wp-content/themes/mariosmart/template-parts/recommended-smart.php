<?php if ($query->have_posts()) : ?>
    <section class="section-recommended">
        <div class="section-title-wrap">
            <h3 class="section-title"><?php echo $title ?? '' ?></h3>
        </div> <!-- end title wrap -->
        <div class="row mt-30">
            <?php while ($query->have_posts()) : $query->the_post(); ?>
                <div class="col-md-3">
                    <article class="entry">
                        <div class="entry__img-holder">
                            <a href="<?php the_permalink(); ?>">
                                <div class="thumb-container">
                                    <img data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="entry__img lazyload" alt="" />
                                </div>
                            </a>
                        </div>

                        <div class="entry__body">
                            <div class="entry__header">
                                <h2 class="entry__title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                            </div>
                            <div class="entry__excerpt">
                                <p><?php echo get_field("description_content", get_the_ID()) ?></p>
                            </div>
                        </div>
                    </article>
                </div>
            <?php endwhile; ?>

        </div>
    </section>
<?php endif; ?>