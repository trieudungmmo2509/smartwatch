<aside class="col-md-4 sidebar sidebar--right">
    <?php wpse_get_template_part("template-parts/sidebar/popular-posts", null, array("current_postID" => $post->ID)); ?>
    <!-- Widget Latest Reviews -->
    <?php wpse_get_template_part("template-parts/sidebar/latest-reviews", null, array("current_postID" => $post->ID)); ?>
    <!-- end widget latest reviews -->

</aside> <!-- end sidebar -->