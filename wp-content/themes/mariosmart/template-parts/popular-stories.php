<?php
$the_query = query_article_home_page("popular_news", "12");
if ($the_query->have_posts()) : ?>
    <section class="content-section section-editors-choice">
        <div class="section-title-wrap">
            <h3 class="section-title">Popular stories</h3>
        </div>

        <div class="row">
            <?php
            $j = 0;
            $i = 0;
            while ($j < 3) :
                $j++;
            ?>
                <div class="col-lg-4">
                    <?php
                    while ($the_query->have_posts()) : $the_query->the_post();
                        $i++;
                    ?>
                        <?php if ($i == 1 || $i == 5 || $i == 9) : ?>
                            <article class="entry">
                                <div class="entry__img-holder">
                                    <a href="single-post.html">
                                        <div class="thumb-container">
                                            <img data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="entry__img lazyload" alt="" />
                                        </div>
                                    </a>
                                </div>

                                <div class="entry__body">
                                    <div class="entry__header">
                                        <h2 class="entry__title">
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h2>
                                    </div>
                                    <div class="entry__excerpt">
                                        <p><?php echo get_field("description_content", get_the_ID()) ?></p>
                                    </div>
                                </div>
                            </article>
                        <?php else : ?>
                            <ul class="post-list-small">
                                <li class="post-list-small__item">
                                    <article class="post-list-small__entry">
                                        <a href="<?php the_permalink(); ?>" class="clearfix">
                                            <div class="post-list-small__img-holder">
                                                <div class="thumb-container">
                                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="post-list-small__img" alt="">
                                                </div>
                                            </div>
                                            <div class="post-list-small__body">
                                                <h3 class="post-list-small__entry-title">
                                                    <?php the_title(); ?>
                                                </h3>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                            </ul>
                        <?php endif; ?>
                        <?php if ($i == 4 || $i == 8)
                            break; ?>
                    <?php endwhile; ?>
                </div>
            <?php endwhile; ?>
        </div>
    </section>
<?php endif; ?>