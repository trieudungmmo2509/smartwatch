<?php
$the_recent_news = query_article_home_page("hot_news", "6");
if ($the_recent_news->have_posts()) : ?>
    <section class="content-section section-recent-news tab-post">
        <div class="section-title-wrap">
            <h3 class="section-title">Recent News</h3>
        </div> <!-- end title wrap -->

        <!-- Tabs Content -->
        <div class="tabs__content tabs__content-trigger tab-post__tabs-content">

            <!-- Pane 1 -->
            <div class="tabs__content-pane tabs__content-pane--active" id="tab-all">
                <div class="row row-16">
                    <?php
                    $i = 0;
                    while ($the_recent_news->have_posts()) : $the_recent_news->the_post();
                        $i++;
                    ?>
                        <div class="col-lg-<?php echo ($i == 2 || $i == 6) ? '6' : '3'; ?> col-sm-6">
                            <article class="entry">
                                <div class="thumb-bg-holder entry__bg-img-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>')">
                                    <a href="<?php the_permalink(); ?>" class="thumb-url"></a>
                                    <div class="bottom-gradient"></div>
                                </div>
                                <div class="thumb-text-holder">
                                    <h2 class="thumb-entry-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                </div>
                            </article>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div> <!-- end pane 1 -->

        </div> <!-- end tabs content -->
    </section>
<?php endif; ?>