<?php
$the_query = query_article_home_page("slider_news", "5");
if ($the_query->have_posts()) : ?>
    <section class="hero">
        <div id="flickity-hero" class="carousel-main">
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="carousel-cell hero__slide">
                    <article class="hero__slide-entry entry">
                        <div class="hero__slide-thumb-bg-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>')">
                            <div class="bottom-gradient"></div>
                        </div>

                        <div class="hero__slide-thumb-text-holder">
                            <div class="container">
                                <a href="categories.html" class="entry__meta-category entry__meta-category--label"><?php echo get_the_category(get_the_ID()); ?></a>
                                <h2 class="entry__title--lg entry__title--white">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                            </div>
                        </div>
                    </article>
                </div>
            <?php endwhile; ?>

        </div> <!-- end flickity -->

        <!-- Slider thumbs -->
        <div class="carousel-thumbs-holder">
            <div class="container">
                <div id="flickity-thumbs" class="carousel-thumbs">
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="carousel-cell">
                            <div class="carousel-thumbs__item">
                                <div class="carousel-thumbs__img-holder">
                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="" style="width: auto; height: 62px;">
                                </div>
                                <h2 class="carousel-thumbs__title">
                                    <?php the_title(); ?>
                                </h2>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>