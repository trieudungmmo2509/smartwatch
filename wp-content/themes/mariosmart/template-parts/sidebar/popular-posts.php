<?php
$query = query_article_sidebar('popular_news', "5", $current_postID);
if ($query->have_posts()) : ?>
    <!-- Widget Popular Posts -->
    <div class="widget widget-popular-posts">
        <h4 class="widget-title">Popular Posts</h4>
        <ul class="widget-popular-posts__list">
            <?php $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <li>
                    <article class="clearfix">
                        <div class="widget-popular-posts__img-holder">
                            <span class="widget-popular-posts__number"><?php echo $i++; ?></span>
                            <div class="thumb-container">
                                <a href="<?php the_permalink(); ?>">
                                    <img data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="" class="lazyload">
                                </a>
                            </div>
                        </div>
                        <div class="widget-popular-posts__entry">
                            <h3 class="widget-popular-posts__entry-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h3>
                        </div>
                    </article>
                </li>
            <?php endwhile; ?>
        </ul>
    </div> <!-- end widget popular posts -->
<?php endif; ?>