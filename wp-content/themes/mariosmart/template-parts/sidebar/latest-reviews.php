<?php
$last_query = query_article_sidebar('tech_reviews', "8");
?>
<div class="widget widget-latest-reviews">
    <h4 class="widget-title">Latest Reviews</h4>
    <?php if ($last_query->have_posts()) : ?>
        <?php $i = 0;
        while ($last_query->have_posts()) : $last_query->the_post();
            $i++;
        ?>
            <?php
            if ($i == 0) :
            ?>
                <article class="entry">
                    <div class="entry__img-holder">
                        <a href="single-post.html">
                            <div class="thumb-container">
                                <img data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="entry__img lazyload" alt="">
                            </div>
                        </a>
                    </div>

                    <div class="entry__body">
                        <div class="entry__header">
                            <h2 class="entry__title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                        </div>
                        <ul class="entry__meta">
                            <li class="entry__meta-rating">
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star-outline"></i>
                            </li>
                        </ul>
                    </div>
                </article>
            <?php else : ?>
                <ul class="post-list-small">
                    <li class="post-list-small__item">
                        <article class="post-list-small__entry">
                            <a href="<?php the_permalink(); ?>" class="clearfix">
                                <div class="post-list-small__img-holder">
                                    <div class="thumb-container">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="post-list-small__img" alt="">
                                    </div>
                                </div>
                                <div class="post-list-small__body">
                                    <h3 class="post-list-small__entry-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    <ul class="entry__meta">
                                        <li class="entry__meta-rating">
                                            <i class="ui-star"></i>
                                            <i class="ui-star"></i>
                                            <i class="ui-star"></i>
                                            <i class="ui-star"></i>
                                            <i class="ui-star"></i>
                                        </li>
                                    </ul>
                                </div>
                            </a>
                        </article>
                    </li>
                </ul>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

</div>