<?php
$the_tech_reviews = query_article_home_page("tech_reviews", "8");
if ($the_tech_reviews->have_posts()) : ?>
    <section class="section-dont-miss">
        <div class="section-title-wrap">
            <h3 class="section-title">Tech Reviews</h3>
        </div>
        <div class="row">
            <?php
            $j = 0;
            $i = 0;
            while ($j < 2) :
                $j++;
            ?>
                <div class="col-md-6 col-xs-12">
                    <?php
                    while ($the_tech_reviews->have_posts()) : $the_tech_reviews->the_post();
                        $i++;
                    ?>
                        <article class="entry post-list">
                            <div class="entry__img-holder post-list__img-holder">
                                <a href="single-post.html">
                                    <div class="thumb-container">
                                        <img data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="entry__img lazyload" alt="" />
                                    </div>
                                </a>
                            </div>

                            <div class="entry__body post-list__body">
                                <div class="entry__header">
                                    <h2 class="entry__title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                </div>
                                <div class="entry__excerpt">
                                    <p><?php echo get_field("description_content", get_the_ID()) ?></p>
                                </div>
                            </div>
                        </article>
                        <?php if ($i == 4)
                            break; ?>
                    <?php endwhile; ?>
                </div>
            <?php endwhile; ?>

        </div>
    </section>
<?php endif; ?>