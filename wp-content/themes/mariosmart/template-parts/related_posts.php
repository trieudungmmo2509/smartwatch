<?php
$related_query = query_article_withby_category($categoryId, "6");
?>
<?php if ($related_query->have_posts()) : ?>
    <div class="related-posts">
        <h5 class="related-posts__title">You might like</h5>
        <div class="row row-20">
            <?php while ($related_query->have_posts()) : $related_query->the_post(); ?>
                <div class="col-md-4">
                    <article class="related-posts__entry entry">
                        <a href="<?php the_permalink(); ?>">
                            <div class="thumb-container">
                                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="" class="entry__img lazyload">
                            </div>
                        </a>
                        <div class="related-posts__text-holder">
                            <h2 class="related-posts__entry-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                        </div>
                    </article>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>