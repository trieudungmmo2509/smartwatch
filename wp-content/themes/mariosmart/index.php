<?php get_header() ?>

<main class="main-container" id="main-container">

    <!-- Hero Slider -->
    <?php get_template_part("template-parts/home-slider", null) ?>
    <!-- end hero slider -->

    <!-- Content -->
    <div class="content container">
        <div class="row">
            <!-- Blog Content -->
            <div class="md-8 blog__content mb-30">
                <?php get_template_part("template-parts/recent-news", null); ?>
                <!-- Recent News -->

                <!-- end recent news -->

                <!-- Editor's Choice -->
                <?php get_template_part("template-parts/popular-stories", null); ?>
                <!-- end editors choice -->

                <!-- Don't Miss -->
                <?php get_template_part("template-parts/tech-reviews", null); ?>
                <!-- end don't miss -->

            </div> <!-- end blog content -->
        </div> <!-- end row -->

        <!-- Ad Banner 728 -->
        <div class="text-center pt-40 pb-40">
            <a href="#">
                <img src="img/blog/placeholder_728.jpg" alt="">
            </a>
        </div>

        <!-- Latest Videos -->
        <section class="section-wrap pt-20">
            <div class="section-title-wrap">
                <h3 class="section-title">Latest Videos</h3>
            </div>

            <div class="video-playlist">

                <div class="video-playlist__content thumb-container">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/mn6Ia5e_suY?feature=oembed" class="video-playlist__content-video">
                        </iframe>
                    </div>
                </div>

                <div class="video-playlist__list">
                    <a href="https://www.youtube.com/embed/mn6Ia5e_suY?feature=oembed&amp;autoplay=1" class="video-playlist__list-item video-playlist__list-item--active">
                        <div class="video-playlist__list-item-thumb thumb-container">
                            <img data-src="https://i.ytimg.com/vi/mn6Ia5e_suY/default.jpg" src="../../../i.ytimg.com/vi/mn6Ia5e_suY/default.jpg" class="video-playlist__list-item-img lazyload" alt="">
                        </div>
                        <div class="video-playlist__list-item-description">
                            <h4 class="video-playlist__list-item-title">Top 5 New Tech Gadgets You Must Have In 2017</h4>
                        </div>
                    </a>
                    <a href="https://www.youtube.com/embed/x68XnWszi2A?feature=oembed&amp;autoplay=1" class="video-playlist__list-item">
                        <div class="video-playlist__list-item-thumb thumb-container">
                            <img data-src="https://i.ytimg.com/vi/x68XnWszi2A/default.jpg" src="../../../i.ytimg.com/vi/mn6Ia5e_suY/default.jpg" class="video-playlist__list-item-img lazyload" alt="">
                        </div>
                        <div class="video-playlist__list-item-description">
                            <h4 class="video-playlist__list-item-title">10 Amazing Gadgets You Can Buy Now On Amazon</h4>
                        </div>
                    </a>
                    <a href="https://www.youtube.com/embed/TXFrCy47ue8?feature=oembed&amp;autoplay=1" class="video-playlist__list-item">
                        <div class="video-playlist__list-item-thumb thumb-container">
                            <img data-src="https://i.ytimg.com/vi/TXFrCy47ue8/default.jpg" src="../../../i.ytimg.com/vi/mn6Ia5e_suY/default.jpg" class="video-playlist__list-item-img lazyload" alt="">
                        </div>
                        <div class="video-playlist__list-item-description">
                            <h4 class="video-playlist__list-item-title">Top 5 Best Drones with HD Camera (Cheap and Affordable Version)</h4>
                        </div>
                    </a>
                    <a href="https://www.youtube.com/embed/rdFXddVsUuQ?feature=oembed&amp;autoplay=1" class="video-playlist__list-item">
                        <div class="video-playlist__list-item-thumb thumb-container">
                            <img data-src="https://i.ytimg.com/vi/rdFXddVsUuQ/default.jpg" src="../../../i.ytimg.com/vi/mn6Ia5e_suY/default.jpg" class="video-playlist__list-item-img lazyload" alt="">
                        </div>
                        <div class="video-playlist__list-item-description">
                            <h4 class="video-playlist__list-item-title">iPhone X Review – Pushing Me to Android</h4>
                        </div>
                    </a>
                </div>

            </div>
        </section> <!-- end latest videos -->

        <!-- Recommended -->
        <?php wpse_get_template_part("template-parts/recommended-smart", null, array("title" => "Smartwatches", "query" => query_article_home_page("smart_watches", "8"))); ?>

        end recommended
        <?php wpse_get_template_part("template-parts/recommended-smart", null, array("title" => "Apple Watch", "query" => query_article_home_page("apple_watch ", "8"))); ?>

        <?php wpse_get_template_part("template-parts/recommended-smart", null, array("title" => "Fitbit", "query" => query_article_home_page("fitbit_news ", "8"))); ?>

        <?php wpse_get_template_part("template-parts/recommended-smart", null, array("title" => "Garmin", "query" => query_article_home_page("garmin_news ", "8"))); ?>
    </div>
    <!-- end content -->

    <!-- Footer -->
    <?php get_footer() ?>
    <!-- end footer -->

</main> <!-- end main container -->
<!-- jQuery Scripts -->
<?php wp_footer(); ?>
</body>

</html>