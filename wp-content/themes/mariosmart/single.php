<?php get_header() ?>
<?php
global $post;
$categories = get_the_category($post->ID)[0];
?>
<main class="main oh" id="main">
    <div class="main-container" id="main-container">

        <!-- Content -->
        <section class="section-wrap pt-60 pb-20">
            <div class="container">
                <div class="row">

                    <!-- post content -->
                    <div class="col-md-8 blog__content mb-30">

                        <!-- standard post -->
                        <article class="entry">

                            <div class="single-post__entry-header  entry__header">

                                <h1 class="single-post__entry-title">
                                    <?php echo $post->post_title; ?>
                                </h1>
                            </div>
                            <div class="entry__article-holder">
                                <!-- Share -->
                                <div class="entry__share">
                                    <div class="entry__share-inner">
                                        <div class="socials">
                                            <a href="#" class="social-facebook entry__share-social" aria-label="facebook"><i class="ui-facebook"></i></a>
                                            <a href="#" class="social-twitter entry__share-social" aria-label="twitter"><i class="ui-twitter"></i></a>
                                            <a href="#" class="social-google-plus entry__share-social" aria-label="google+"><i class="ui-google"></i></a>
                                            <a href="#" class="social-instagram entry__share-social" aria-label="instagram"><i class="ui-instagram"></i></a>
                                        </div>
                                    </div>
                                </div> <!-- share -->

                                <div class="entry__article">
                                    <?php echo $post->post_content; ?>
                                </div> <!-- end entry article -->
                            </div>

                            <!-- Related Posts -->
                            <?php wpse_get_template_part("template-parts/related_posts", null, array("categoryId" => $categories->term_id)); ?>
                            <!-- end related posts -->

                        </article> <!-- end standard post -->



                        <!-- Comments -->
                        <div class="entry-comments mt-30">
                            <h5 class="entry-comments__title">3 comments</h5>

                            <ul class="comment-list">
                                <li class="comment">
                                    <div class="comment-body">
                                        <div class="comment-avatar">
                                            <img alt="" src="img/blog/comment_1.png">
                                        </div>
                                        <div class="comment-text">
                                            <h6 class="comment-author">Joeby Ragpa</h6>
                                            <div class="comment-metadata">
                                                <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                                            </div>
                                            <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                                            <a href="#" class="comment-reply">Reply</a>
                                        </div>
                                    </div>

                                    <ul class="children">
                                        <li class="comment">
                                            <div class="comment-body">
                                                <div class="comment-avatar">
                                                    <img alt="" src="img/blog/comment_2.png">
                                                </div>
                                                <div class="comment-text">
                                                    <h6 class="comment-author">Alexander Samokhin</h6>
                                                    <div class="comment-metadata">
                                                        <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                                                    </div>
                                                    <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                                                    <a href="#" class="comment-reply">Reply</a>
                                                </div>
                                            </div>
                                        </li> <!-- end reply comment -->
                                    </ul>

                                </li> <!-- end 1-2 comment -->

                                <li>
                                    <div class="comment-body">
                                        <div class="comment-avatar">
                                            <img alt="" src="img/blog/comment_3.png">
                                        </div>
                                        <div class="comment-text">
                                            <h6 class="comment-author">Chris Root</h6>
                                            <div class="comment-metadata">
                                                <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                                            </div>
                                            <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                                            <a href="#" class="comment-reply">Reply</a>
                                        </div>
                                    </div>
                                </li> <!-- end 3 comment -->

                            </ul>
                        </div> <!-- end comments -->


                        <!-- Comment Form -->
                        <div id="respond" class="comment-respond">
                            <h5 class="comment-respond__title">Post a comment</h5>
                            <p class="comment-respond__subtitle">Your email address will not be published. Required fields are marked*</p>
                            <form id="form" class="comment-form" method="post" action="#">
                                <p class="comment-form-comment">
                                    <label for="comment">Comment</label>
                                    <textarea id="comment" name="comment" rows="5" required="required"></textarea>
                                </p>

                                <div class="row row-20">
                                    <div class="col-lg-4">
                                        <label for="name">Name*</label>
                                        <input name="name" id="name" type="text">
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="email">Email*</label>
                                        <input name="email" id="email" type="email">
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="email">Website</label>
                                        <input name="website" id="website" type="text">
                                    </div>
                                </div>

                                <p class="comment-form-submit">
                                    <input type="submit" class="btn btn-lg btn-color btn-button" value="Post Comment" id="submit-message">
                                </p>

                            </form>
                        </div> <!-- end comment form -->

                    </div> <!-- end col -->

                    <!-- Sidebar -->
                    <?php wpse_get_template_part("template-parts/sidebar-article", null, array("current_postID" => $post->ID)); ?>
                    <!-- end sidebar -->

                </div> <!-- end row -->
            </div> <!-- end container -->
        </section> <!-- end content -->

        <?php get_footer() ?>

</main> <!-- end main-wrapper -->
<!-- jQuery Scripts -->
<?php wp_footer(); ?>
</body>

</html>